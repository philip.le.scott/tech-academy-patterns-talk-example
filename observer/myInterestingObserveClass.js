const MyInterestingEmitClass = require('./MyInterestingEmitClass');
const Observer = require('./Observer');
class MyInterestingObserveClass {
  constructor( ) {
    this.emitClass = new MyInterestingEmitClass();
    this.Observers = [];
    this.Observers['anInterestingAction'] = new Observer( (index) => this.eventObserved(index) )

    this.emitClass.registerEventObserver(
      'myInterestingAction',
      this.Observers.anInterestingAction
    )
  }
  eventObserved( index ) {
    console.log( 'Emit class observed.', index );
  }
}
module.exports = MyInterestingObserveClass;
