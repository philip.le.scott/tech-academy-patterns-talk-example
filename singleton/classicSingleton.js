var mySingleton = (function () {
  // Instance stores a reference to the Singleton
  var instance;

  function init() {
    // Private methods and variables
    var myVar = 'Default value';

    return {
      // Public methods and variables
      getMyVar: function () {
        return myVar;
      },
      setMyVar: function( val ) {
        myVar = val;
      }
    };
  };

  return {
    // Get the Singleton instance if one exists
    // or create one if it doesn't
    getInstance: function () {
      if ( !instance ) {
        instance = init();
      }
      return instance;
    }
  };
})();
module.exports = mySingleton;
