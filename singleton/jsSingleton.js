const MySingleton = (function(){
  let myVar = 'Default value!';
  return {
    getMyVar: function() {
      return myVar;
    },
    setMyVar: function( val ) {
      myVar = val;
    }
  }
})();
module.exports = MySingleton;
